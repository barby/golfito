package juego;
	
import java.awt.Color;

import entorno.Entorno;

public class Hoyo {
	private double x;
	private double y;
	
   
	Hoyo (double x, double y){
		this.x=x;
		this.y=y;
	}
	void dibujarHoyo(Entorno entorno){
		entorno.dibujarCirculo(this.x, this.y, 20, Color.BLACK);
	}
	public double getX(){
		return this.x;
	}
	public double getY(){
		return this.y;
		
	}

}
