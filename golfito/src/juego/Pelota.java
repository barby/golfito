package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Pelota {
	private double x;
	private double y;
	private double angulo;
	private double velocidad;
	private Image foto3;
   
	Pelota (double x, double y,double angulo, double velocidad){
		this.x=x;
		this.y=y;
		this.angulo =angulo;
		this.velocidad=velocidad;
		this.foto3 = Herramientas.cargarImagen("51193.gif");
		
	}
	void dibujarPelota(Entorno entorno){
		entorno.dibujarCirculo(this.x, this.y, 15, Color.white);
		entorno.dibujarImagen(foto3, x, y, 0 , 0.2);
	}
	void mover(){
		this.x= this.x+(this.velocidad*Math.cos(this.angulo));
		this.y= this.y+(this.velocidad*Math.sin(this.angulo));
	}
	void frenar(){
		this.velocidad=this.velocidad-0.02;
	}

	void cambiarAngulo(){
		this.angulo=this.angulo+Math.PI/2;
	}
	
	public double getAngulo(){
		return this.angulo;
	}
	public double getVelocidad(){
		return this.velocidad;
	}
	public double getX(){
		return this.x;
	}
	public double getY(){
		return this.y;
	}
	public void setVelocidad(double velocidad){
		this.velocidad=velocidad;
	}
	public void setAngulo(double angulo){
		this.angulo=angulo;
	}
	public void setX(double x){
		this.x=x;
	}
	public void setY(double y){
		this.x=y;
	}

}
