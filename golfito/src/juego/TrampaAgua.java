package juego;
import java.awt.Color;
import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class TrampaAgua {
	private double x;
	private double y;
	private double alto;
	private double ancho;
	private Image foto1;


	TrampaAgua(double x, double y) {
		this.x=x;
		this.y=y;
		this.alto=48;
		this.ancho=104;	
		this.foto1 = Herramientas.cargarImagen("gif3.gif");
	}
	void dibujarTrampaAgua(Entorno entorno){
		entorno.dibujarRectangulo(this.x, this.y, this.ancho, alto, 0, Color.BLUE);
		entorno.dibujarImagen(foto1, x, y, 0, 0.18);
	}
	public double getX(){
		return this.x;
	}
	public double getY(){
		return this.y;	
		}
	public double getAncho(){
		return this.ancho;
	}
	public double getAlto(){
		return this.alto;
	}
}
