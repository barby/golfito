package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Flecha {
	private double x;
	private double y;
	private double alto;
	private double ancho;
	private double angulo;
	private Image foto4;
	
	Flecha(double x,double y){
		this.x=x;
		this.y=y;
		this.alto=2;
		this.ancho=40;
		this.angulo=0;
		this.foto4=Herramientas.cargarImagen("flecha-imagen-animada-0046.gif");
		
	}
	void dibujar (Entorno entorno){
		entorno.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, this.angulo, Color.red);
		entorno.dibujarImagen(foto4, x, y, this.angulo, 0.4);
	}
	void moverFlechaDer (){
		this.angulo+=0.05;
	}
	void moverFlechaIzq(){
		this.angulo-=0.05;
	}
	
	public double getX(){
		return this.x;
	}
	public double getY(){
		return this.y;
	}
	public double getAngulo(){
		return this.angulo;
	}
	public void setY(double y){
		this.y=y;
	}
	public void setX(double x){
		this.x=x;
	}	
	public void setAngulo(double angulo){
		this.angulo=angulo;
	}
}