package juego;

import java.awt.Color;

import entorno.Entorno;

public class Potencia {
	private double x;
	private double y;
	private double alto;
	Potencia(double x,double y,double alto) {
		this.x=x;
		this.y=y;
		this.alto=alto;
		
	}
	void dibujarBarra(Entorno entorno){
		entorno.dibujarRectangulo(this.x, this.y, 20, this.alto, 0, Color.red);
	}
	void aumentarPotencia(){
		if(this.alto<50)
			this.alto+=0.2;
	}
	public double getX(){
		return this.x;
	}
	public double getY(){
		return this.y;
	}
	public double getAlto(){
		return this.alto;
	}
	 public void setAlto(double alto){
		this.alto=alto;
	}
}
