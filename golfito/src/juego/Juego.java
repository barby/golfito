package juego;


import java.awt.Image;
import java.util.Random;

import javax.sound.sampled.Clip;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego
{
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private Pelota pelotita;
	private Hoyo agujero;
	private Flecha direccion;
	private Potencia barrita;
	private TrampaAgua [] estanque ;
	private TrampaArena [] arena;
	private Image imag;
	private Clip sonido;

	// Variables y métodos propios de cada grupo
	// ...
	
	
	Juego()
	{
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Golfito - Grupo Apellido1 - Apellido2 -Apellido3 - V0.01", 800, 600);
		
		this.pelotita= new Pelota(50,50,0,0);
		this.agujero=new Hoyo(750,550);
		this.direccion= new Flecha (pelotita.getX(),pelotita.getY());
		this.barrita= new Potencia(25,580,1);
		this.arena= new TrampaArena[3];
		this.estanque= new TrampaAgua [3];
		
		this.imag= Herramientas.cargarImagen("pasto2.jpg");
		this.sonido=Herramientas.cargarSonido("Game-of-thrones.wav");
		
		// Inicializar lo que haga falta para el juego
		// ...

		// Inicia el juego!
		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y 
	 * por lo tanto es el método más importante de esta clase. Aquí se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	public void tick()
	{
	 this.sonido.loop(this.sonido.LOOP_CONTINUOUSLY);
	 this.sonido.start();
	 entorno.dibujarImagen(this.imag, 400, 300, 0, 1);
	 this.pelotita.dibujarPelota(entorno); 
	 this.agujero.dibujarHoyo(entorno);
	 crearTrampaArena(arena);
	 crearTrampaAgua(estanque);
	 dibujarTrampas(arena,estanque);
	 
	//movimiento de la flecha 
	 if (entorno.estaPresionada(entorno.TECLA_DERECHA)){
		 direccion.moverFlechaDer();
	 }
	 if  (entorno.estaPresionada(entorno.TECLA_IZQUIERDA)){
		direccion.moverFlechaIzq(); 
	 }
	  // la pelota esta en movimiento y se frenara gradualmente.
	 
 	
	if (entorno.sePresiono(entorno.TECLA_ESPACIO) || entorno.estaPresionada(entorno.TECLA_ESPACIO)){
		 barrita.dibujarBarra(entorno);
		 barrita.aumentarPotencia();
		 pelotita.setAngulo(direccion.getAngulo());
		 pelotita.setVelocidad(barrita.getAlto()/2); 
	 }
	else{
		this.pelotita.mover();
		this.barrita.setAlto(0);
	}
	if(estaEnMovimiento(pelotita)){
		this.pelotita.frenar();
		if(tocaPared(pelotita)){
	 		this.pelotita.cambiarAngulo();
	 		this.pelotita.mover();
	 	}
	}
	else{
		this.pelotita.setVelocidad(0);
		this.pelotita.setAngulo(0);
		this.direccion.setX(this.pelotita.getX());
		this.direccion.setY(this.pelotita.getY());
		this.direccion.dibujar(entorno);
	}
}

	
	//-----------------------------------------------------//
	//CREACION DE TRAMPAS
	public void crearTrampaArena(TrampaArena [] arena){
		Random rand= new Random();
		int posicionX= rand.nextInt(700);
		int posicionY= rand.nextInt(500);
		for (int i = 0; i < arena.length; i++) {
			if (arena[i]==null){
					arena[i]= new TrampaArena(posicionX,posicionY);
					break;
			}
		}
	}
		public void crearTrampaAgua(TrampaAgua[] agua){
			Random rand=new Random();
			int posicionX=rand.nextInt(700);
			int posicionY=rand.nextInt(500);
			for (int i = 0; i < agua.length; i++) {
				if(agua[i]==null){
					agua[i]=new TrampaAgua (posicionX,posicionY);					
					break;
				}
				
			}
		}
	
	public void dibujarTrampas(TrampaArena [] arena,TrampaAgua []agua){
		for (int i = 0; i < arena.length; i++) {
			if (arena[i]!=null && agua[i]!= null){
				arena[i].dibujarTrampaArena(entorno);
				agua[i].dibujarTrampaAgua(entorno);
			}
		}
	}
	// sector de verificación
	boolean tocaPared(Pelota p){
		return p.getX()>this.entorno.ancho()-5 || p.getX() <5 || p.getY()<5 || p.getY()>this.entorno.alto()-15;
	}
	boolean estaEnMovimiento(Pelota p){
		if( p.getVelocidad()>0)
			return true;
		return false;
	}
	boolean estaEnTrampaArena(Pelota p,TrampaArena trampa){
		return(p.getX()>=izquierdoArena(trampa) && p.getX()<=derechoArena(trampa) && 
			   p.getY()<=inferiorArena(trampa)  && p.getY()<=superiorArena(trampa));
	}
	boolean estaEnTrampaAgua(Pelota p,TrampaAgua trampa){
		return(p.getX()>=izquierdoAgua(trampa) && p.getX()<=derechoAgua(trampa) && 
				   p.getY()<=inferiorAgua(trampa)  && p.getY()<=superiorAgua(trampa));
		}
	
	//Bloque para saber el contorno de los objetos
	public double superiorArena(TrampaArena trampa){
		double partSup=(trampa.getY())-(trampa.getAlto()/2)-11;
		return partSup;
	}
	public double inferiorArena(TrampaArena trampa){
		double partInf= (trampa.getAlto()/2)+(trampa.getY());
		return partInf;
	}
	public double derechoArena(TrampaArena trampa){
		double partDer= (trampa.getAncho()/2)+(trampa.getX())+6;
		return partDer;
	}
	public double izquierdoArena(TrampaArena trampa){
		double partIzq=(trampa.getX())-(trampa.getAncho()/2)-6;
		return partIzq;
	}
	public double superiorAgua(TrampaAgua trampa){
		double partSup=(trampa.getY())-(trampa.getAlto()/2)-11;
		return partSup;
	}
	public double inferiorAgua(TrampaAgua trampa){
		double partInf= (trampa.getAlto()/2)+(trampa.getY());
		return partInf;
	}
	public double derechoAgua(TrampaAgua trampa){
		double partDer= (trampa.getAncho()/2)+(trampa.getX())+6;
		return partDer;
	}
	public double izquierdoAgua(TrampaAgua trampa){
		double partIzq=(trampa.getX())-(trampa.getAncho()/2)-6;
		return partIzq;
	}

	
	@SuppressWarnings("unused")
	public static void main(String[] args)
	{
		Juego juego = new Juego();
	}
}
